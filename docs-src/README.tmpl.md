# pybased

Library for creating arbitrary binary encodings.  Includes variations on base32, base64, base85, and more.

**WARNING:** Although these encodings *do* work end-to-end, they are not compatible with traditional implementations!

I am working on finding out why.

## Encodings

### Types

 * **Sliding:** Random-access bitwise implementation, theoretically compatible with bytestrings of arbitrary size.
 * **BigInt:** Converts bytes into large integers, then incrementally divides by the radix. Incompatible with large files. Suitable for hashes and shorter bytestrings.
 * **Biterator:** Iterates through each byte from the input stream and adds it to a buffer, then extracts the required number of bits to convert to the other radix. Based roughly off of CPython's base64 encoder, but heavily modified to enable arbitrary conversions. **WIP.**

### Available Encodings

{{ AVAILABLE_ENCODINGS }}

### Status

{{ SELF_TEST }}

## Getting started

```shell
$ pip install pybased
```

## Doing stuff
```python
# Lets's assume we want to use the Crockford32 encoding scheme.
from based.standards.base32 import crockford32

# And let's assume the variable data has what we want to encode.
data: bytes = ...

# Encode to string.
encoded: str = crockford32.encode_bytes(data)

# ...

# Decode the string back to bytes.
data: bytes = crockford32.decode_bytes(encoded)
```

## `based` Command-Line Tool

{{ cmd(['based', '--help'], echo=True) }}

**NOTE:** The `based` CLI tool is currently only useful for testing, and is under *very* active development.

### Encode string to Base94

{{ cmd(['based', 'encode', '--standard=base94', '--input-string', 'Hello, world!'], echo=True) }}

### Encode string to Base94 and output JSON

{{ cmd(['based', 'encode', '--standard=base94', '--input-string', 'Hello, world!', '--output-format', 'json'], echo=True) }}

### Decode data from Base94

{{ cmd(['based', 'decode', '--standard=base94', '--input-string', '/P|?l:+>Nq\sr<+r'], echo=True) }}

## Various Output Encoding Formats

{{ cmd(['based', 'decode', '--standard=base94', '--input-string', '/P|?l:+>Nq\sr<+r', '--output-format', 'json'], echo=True) }}
{{ cmd(['based', 'decode', '--standard=base94', '--input-string', '/P|?l:+>Nq\sr<+r', '--output-format', 'yaml'], echo=True) }}