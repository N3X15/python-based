import unittest
from tests.standards.test_base32 import *
from tests.standards.test_base62 import *
from tests.standards.test_base64 import *
from tests.standards.test_base85 import *
from tests.standards.test_base94 import *

if __name__ == "__main__":
    unittest.main()