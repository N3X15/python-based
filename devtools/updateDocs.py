import json
from typing import Any, Dict, List, Pattern
from buildtools.maestro.base_target import SingleBuildTarget
from buildtools.maestro.jinja2 import Jinja2BuildTarget, j_cmd
from buildtools.maestro import BuildMaestro
from buildtools import os_utils, log

import re,os,sys,jinja2
from pathlib import Path
from ruamel.yaml import YAML

yaml = YAML(typ='rt')

_env = os_utils.ENV.clone()

DIST_DIR = Path('dist')
BIN_DIR = DIST_DIR / 'bin'
DATA_DIR = Path('data')
DOC_SRC_DIR = Path('docs-src')
BASED_DIR = Path('based')
BASED_EXE: Path = BIN_DIR / ('based' + ('.exe' if os_utils.is_windows() else ''))

BASED: str = 'based'
if BASED_EXE.is_file():
    BASED = str(BASED_EXE)
_jenv = jinja2.Environment(
    loader=jinja2.FileSystemLoader(DOC_SRC_DIR),
    extensions=['jinja2.ext.do'],
    autoescape=False
)

AVAILABLE_ENCODINGS = os_utils.cmd_out(['based', 'dump', '--format=readme.md'], critical=True, globbify=False)
SELF_TEST           = os_utils.cmd_out(['based', 'encode', '--output-format=readme.md', '--input-string=Hello, world!'], critical=True, globbify=False)
# ENCODING_STATE: Dict[str, Any] = {}
# with open('encoding-states.json', 'r') as f:
#     ENCODING_STATE      = json.load(f)
#print(repr(AVAILABLE_ENCODINGS))
_jenv.globals.update(cmd=j_cmd, AVAILABLE_ENCODINGS=AVAILABLE_ENCODINGS, SELF_TEST=SELF_TEST)
FAILED = False

bm = BuildMaestro()
bm.add(Jinja2BuildTarget('README.tmpl.md', 'README.md', _jenv, {}))
bm.as_app()